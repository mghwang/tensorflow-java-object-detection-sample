# README #

* TensorFlow Java API를 이용한 Object Detection 샘플입니다.

### 관련 링크 ###

* [TensorFlow for Java](https://www.tensorflow.org/install/install_java)
* [TensorFlow core API v1.5.0](https://github.com/tensorflow/tensorflow/tree/v1.5.0/tensorflow/core/api_def/base_api)
* [TensorFlow Detection Model Zoo](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md)
* [TensorFlow: How to freeze a model and serve it with a python API](https://blog.metaflow.fr/tensorflow-how-to-freeze-a-model-and-serve-it-with-a-python-api-d4f3596b3adc)
* [TensorFlow Lite](https://www.tensorflow.org/mobile/tflite/)
* [TensorFlow Lite Repository](https://github.com/tensorflow/tensorflow/tree/master/tensorflow/contrib/lite)
