import com.google.protobuf.TextFormat;
import org.apache.commons.io.IOUtils;
import org.tensorflow.*;
import org.tensorflow.types.UInt8;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public interface ObjectDetector {
    public boolean hasModel(String modelName);
    public DetectionResult detect(String modelName, byte[] imageBytes, float scoreThreshold);
    public String getLabelForIndex(String modelName, int index);

    public class DetectionResult {
        DetectionResult(int sourceImageWidth, int sourceImageHeight) {
            this.sourceImageWidth = sourceImageWidth;
            this.sourceImageHeight = sourceImageHeight;
            this.detectedObjects = new ArrayList<>();
        }
        public int sourceImageWidth;
        public int sourceImageHeight;
        public List<DetectedObject> detectedObjects;    // list of detected objects sorted in score-descending order

    }
    public class DetectedObject {
        DetectedObject(int labelIndex, float score, float bbMinX, float bbMinY, float bbMaxX, float bbMaxY) {
            this.labelIndex = labelIndex;
            this.score = score;
            this.boundingBox = new Rectangle2D.Float(bbMinX, bbMinY, bbMaxX-bbMinX, bbMaxY-bbMinY);
        }
        public int labelIndex;
        public Rectangle2D boundingBox; // bounding box in normalized coordinates (all coordinate values are in [0,1])
        public float score; // score in range [0,1]
    }

    public class ObjectDetectorImpl implements ObjectDetector {
        private Map<String, ModelInfo> modelInfoMap;

        public ObjectDetectorImpl() throws IOException {
            this.modelInfoMap = new HashMap<>();
            modelInfoMap.put(
                    "coco_ssd_mobilenet_v1",
                    new ModelInfo(
                            Paths.get("/Users/1002525/detector_models/coco/mscoco_label_map.pbtxt"),
                            Paths.get("/Users/1002525/detector_models/coco/ssd_mobilenet_v1_coco_2017_11_17/frozen_inference_graph.pb")));
        }

        @Override
        public boolean hasModel(String modelName) {
            return modelInfoMap.containsKey(modelName);
        }

        @Override
        public DetectionResult detect(String modelName, byte[] imageBytes, float scoreThreshold) {
            ModelInfo modelInfo = modelInfoMap.get(modelName);
            if(modelInfo == null) return null;

            long startTime = System.currentTimeMillis();
            try (Tensor<UInt8> decodedImage = decodeImageAndReturnAsTensor(imageBytes)) {
                try (Session s = new Session(modelInfo.detectionGraph)) {
                    List<Tensor<?>> result = s.runner().feed("image_tensor:0", decodedImage)
                            .fetch("detection_boxes:0")
                            .fetch("detection_scores:0")
                            .fetch("detection_classes:0")
                            .fetch("num_detections:0")
                            .run();
                    try (Tensor<Float> boxes = result.get(0).expect(Float.class);
                         Tensor<Float> scores = result.get(1).expect(Float.class);
                         Tensor<Float> classes = result.get(2).expect(Float.class);
                         Tensor<Float> numDetections = result.get(3).expect(Float.class)) {
                        long[] rshape = numDetections.shape();
                        if(rshape.length != 1 || rshape[0] != 1) {
                            throw new RuntimeException("The result tensor shape should be [1]. Actual shape is " + Arrays.toString(rshape));
                        }
                        int numDetectionsValue = (int)numDetections.copyTo(new float[1])[0];

                        float[][] boxesValue = boxes.copyTo(new float[1][numDetectionsValue][4])[0];
                        // Note: Each boxes are in a form [ymin, xmin, ymax, xmax] in normalized coordinates

                        float[] scoresValue = scores.copyTo(new float[1][numDetectionsValue])[0];
                        float[] classesValue = classes.copyTo(new float[1][numDetectionsValue])[0];

                        long[] imageShape = decodedImage.shape();
                        DetectionResult detectionResult = new DetectionResult((int)imageShape[2], (int)imageShape[1]);
                        for(int idx = 0 ; idx < numDetectionsValue; idx++) {
                            if(scoresValue[idx] >= scoreThreshold) {
                                // ssd_mobilenet_v1_coco_2017_11_17 모델로 실험한 결과 score에 대해 decreasing order로 리턴되고 있기 때문에,
                                // 실제로는 threshold 보다 적을 경우 break 해버려도 될 것 같지만, 다른 모델들도 sorting된 상태로 돌려준다는 보장이 없어서 전체 비교를 수행함.
                                detectionResult.detectedObjects.add(new DetectedObject(
                                        (int) classesValue[idx], scoresValue[idx], boxesValue[idx][1], boxesValue[idx][0], boxesValue[idx][3], boxesValue[idx][2]));
                            }
                        }
//                        log.info("Object detection with model {} took {} seconds and returned {} objects (Threshold: {})"
//                                , modelName, ((System.currentTimeMillis()-startTime)/100)/10.0, detectionResult.detectedObjects.size(), scoreThreshold);
                        return detectionResult;
                    }
                }
            }
        }

        @Override
        public String getLabelForIndex(String modelName, int index) {
            ModelInfo modelInfo = modelInfoMap.get(modelName);
            if(modelInfo == null) return null;
            return modelInfo.labelMap.get(index);
        }

        private class ModelInfo {
            Map<Integer, String> labelMap;
            Graph detectionGraph;

            ModelInfo(Path labelMapPath, Path detectionGraphPath) throws IOException {
                byte[] graphDef = IOUtils.toByteArray(Files.newInputStream(detectionGraphPath));
                String labelMapText = IOUtils.toString(Files.newInputStream(labelMapPath), Charset.forName("UTF-8"));
                this.detectionGraph = new Graph();
                this.detectionGraph.importGraphDef(graphDef);
                this.labelMap = new HashMap<>();
                if (labelMapPath.toString().endsWith(".pbtxt")) {
                    Pbtxt.PbtxtFile.Builder pbtxtBuilder = Pbtxt.PbtxtFile.newBuilder();
                    TextFormat.merge(labelMapText, pbtxtBuilder);
                    Pbtxt.PbtxtFile labelDictionary = pbtxtBuilder.build();
                    for (Pbtxt.Item item : labelDictionary.getItemList())
                        labelMap.put(item.getId(), item.hasDisplayName() ? item.getDisplayName() : item.getName());
                } else {
                    // 그 외 경우는 텍스트 파일로 간주
                    int idx = 0;
                    for (String curLine : labelMapText.split("\\n"))
                        labelMap.put(idx++, curLine.trim());
                }
            }
        }

        private static final byte[] jpegPrefix = {(byte)0xff, (byte)0xd8, (byte)0xff};
        private static final byte[] pngPrefix = {(byte)0211, 'P', 'N'};
        private static final byte[] gifPrefix = {(byte)0x47, (byte)0x49, (byte)0x46};
        private static final byte[] bmpPrefix = {'B', 'M'};

        private boolean comparePrefix(byte[] ba, byte[] prefix) {
            int len = prefix.length;
            for(int idx = 0; idx < len ; idx++) if(ba[idx] != prefix[idx]) return false;
            return true;
        }

        private Tensor<UInt8> decodeImageAndReturnAsTensor(byte[] imageBytes) {
            try (Graph g = new Graph()) {
                GraphBuilder b = new GraphBuilder(g);

                Output<String> input = b.constant("input", imageBytes);
                Output<UInt8> output = null;
                if(comparePrefix(imageBytes, jpegPrefix))
                    output = b.expandDims(b.cast(b.decodeJpeg(input, 3), UInt8.class), b.constant("make_batch", 0));
                else if(comparePrefix(imageBytes, pngPrefix))
                    output = b.expandDims(b.cast(b.decodePng(input, 3), UInt8.class), b.constant("make_batch", 0));
                else if(comparePrefix(imageBytes, gifPrefix))
                    output = b.slice(b.cast(b.decodeGif(input, 3), UInt8.class)
                            , b.constant("begin", new int[]{0,0,0,0})
                            , b.constant("size", new int[]{1, -1, -1, -1}));    // DecodeGif 결과 shape는 [numFrames, height, weight, channels(3)] 이므로  [1, height, weight, channels] 형태로 만들기 위해 Slice를 사용
                else {
                    // TODO: 잘못된 input에 대한 exception 처리 방향이 결정되면, bmpPrefix와 비교하여 일치하는 경우만 decodeBmp를 수행하고, 그 외 경우에 대해서는 예외 발생하도록 해야함.
                    output = b.expandDims(b.cast(b.decodeBmp(input, 3), UInt8.class), b.constant("make_batch", 0));
                }
                try (Session s = new Session(g)) {
                    return s.runner().fetch(output.op().name()).run().get(0).expect(UInt8.class);
                }
            }
        }

        private class GraphBuilder {
            GraphBuilder(Graph g) {
                this.g = g;
            }

            Output<Float> div(Output<Float> x, Output<Float> y) {
                return binaryOp("Div", x, y);
            }

            <T> Output<T> sub(Output<T> x, Output<T> y) {
                return binaryOp("Sub", x, y);
            }

            <T> Output<Float> resizeBilinear(Output<T> images, Output<Integer> size) {
                return binaryOp3("ResizeBilinear", images, size);
            }

            <T> Output<T> expandDims(Output<T> input, Output<Integer> dim) {
                return binaryOp3("ExpandDims", input, dim);
            }

            <T> Output<T> slice(Output<T> input, Output<Integer> beginIndices, Output<Integer> sizes) {
                return g.opBuilder("Slice", "Slice")
                        .addInput(input)
                        .addInput(beginIndices)
                        .addInput(sizes)
                        .build()
                        .<T>output(0);
            }

            <T, U> Output<U> cast(Output<T> value, Class<U> type) {
                DataType dtype = DataType.fromClass(type);
                return g.opBuilder("Cast", "Cast")
                        .addInput(value)
                        .setAttr("DstT", dtype)
                        .build()
                        .<U>output(0);
            }

            Output<UInt8> decodeJpeg(Output<String> contents, long channels) {
                return g.opBuilder("DecodeJpeg", "DecodeJpeg")
                        .addInput(contents)
                        .setAttr("channels", channels)
                        .build()
                        .<UInt8>output(0);
            }

            Output<UInt8> decodePng(Output<String> contents, long channels) {
                return g.opBuilder("DecodePng", "DecodePng")
                        .addInput(contents)
                        .setAttr("channels", channels)
                        .build()
                        .<UInt8>output(0);
            }

            Output<UInt8> decodeGif(Output<String> contents, long channels) {
                return g.opBuilder("DecodeGif", "DecodeGif")
                        .addInput(contents)
                        //.setAttr("channels", channels)  // Actually, DecodeGif does not accept channels attribute. always 3 channels.
                        .build()
                        .<UInt8>output(0);
            }

            Output<UInt8> decodeBmp(Output<String> contents, long channels) {
                return g.opBuilder("DecodeBmp", "DecodeBmp")
                        .addInput(contents)
                        .setAttr("channels", channels)
                        .build()
                        .<UInt8>output(0);
            }

            <T> Output<T> constant(String name, Object value, Class<T> type) {
                try (Tensor<T> t = Tensor.<T>create(value, type)) {
                    return g.opBuilder("Const", name)
                            .setAttr("dtype", DataType.fromClass(type))
                            .setAttr("value", t)
                            .build()
                            .<T>output(0);
                }
            }
            Output<String> constant(String name, byte[] value) {
                return this.constant(name, value, String.class);
            }

            Output<Integer> constant(String name, int value) {
                return this.constant(name, value, Integer.class);
            }

            Output<Integer> constant(String name, int[] value) {
                return this.constant(name, value, Integer.class);
            }

            Output<Float> constant(String name, float value) {
                return this.constant(name, value, Float.class);
            }

            private <T> Output<T> binaryOp(String type, Output<T> in1, Output<T> in2) {
                return g.opBuilder(type, type).addInput(in1).addInput(in2).build().<T>output(0);
            }

            private <T, U, V> Output<T> binaryOp3(String type, Output<U> in1, Output<V> in2) {
                return g.opBuilder(type, type).addInput(in1).addInput(in2).build().<T>output(0);
            }
            private Graph g;
        }

    }

    public static void main(String args[]) throws java.io.IOException{
        if(args.length <= 0) {
            System.err.println("Usage : ObjectDetector IMAGE_FILE");
            System.exit(1);
        }
        ObjectDetector od = new ObjectDetectorImpl();
        DetectionResult detectionResult = od.detect(
                "coco_ssd_mobilenet_v1",
                IOUtils.toByteArray(Files.newInputStream(Paths.get(args[0]))),
                0.5f);
        System.out.println("Detection result is as follows:");
        System.out.println("Source size : " + detectionResult.sourceImageWidth + "x" + detectionResult.sourceImageHeight);
        System.out.println("Objects:");
        for(DetectedObject detectedObject : detectionResult.detectedObjects) {
            System.out.println(" " + od.getLabelForIndex("coco_ssd_mobilenet_v1", detectedObject.labelIndex) + "(" + detectedObject.score + ") : " + detectedObject.boundingBox);
        }
        System.exit(0);
    }
}
